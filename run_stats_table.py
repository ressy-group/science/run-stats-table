#!/usr/bin/env python

"""
Gather up base counts per-tile and per-cycle from Illumina stats files.
"""

import re
import struct
import argparse
import xml.etree.ElementTree as ET
from pathlib import Path
from csv import DictWriter

STATS_KEYS = ["cycle", "avg_intensity"] + \
    [f"avg_int_all_{base}" for base in ["A", "C", "G", "T"]] + \
    [f"avg_int_cluster_{base}" for base in ["A", "C", "G", "T"]] + \
    [f"num_clust_call_{base}" for base in ["A", "C", "G", "T", "X"]] + \
    [f"num_clust_int_{base}" for base in ["A", "C", "G", "T"]]
RUN_ATTRS_KEYS = ["run_id", "run_num", "run_flowcell", "run_instrument", "run_date"]
READ_ATTRS_KEYS = ["read_number", "read_is_index"]

# https://support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq_letterbooklet_15038058brpmi.pdf
#
# Start    Description                                                    Data type
# Byte 0   Cycle number                                                   integer
# Byte 4   Average Cycle Intensity                                        double
# Byte 12  Average intensity for A over all clusters with intensity for A double
# Byte 20  Average intensity for C over all clusters with intensity for C double
# Byte 28  Average intensity for G over all clusters with intensity for G double
# Byte 36  Average intensity for T over all clusters with intensity for T double
# Byte 44  Average intensity for A over clusters with base call A         double
# Byte 52  Average intensity for C over clusters with base call C         double
# Byte 60  Average intensity for G over clusters with base call G         double
# Byte 68  Average intensity for T over clusters with base call T         double
# Byte 76  Number of clusters with base call A                            integer
# Byte 80  Number of clusters with base call C                            integer
# Byte 84  Number of clusters with base call G                            integer
# Byte 88  Number of clusters with base call T                            integer
# Byte 92  Number of clusters with base call X                            integer
# Byte 96  Number of clusters with intensity for A                        integer
# Byte 100 Number of clusters with intensity for C                        integer
# Byte 104 Number of clusters with intensity for G                        integer
# Byte 108 Number of clusters with intensity for T                        integer
def load_bcl_stats(path):
    """Load a single BCL stats file into a dictionary.
    See Illumina's bcl2fastq documentation for details on the values.  Note
    that cycle number here is zero-indexed while in the directory names it's
    indexed by one.
    """
    fmt = "<IdddddddddIIIIIIIII"
    with open(path, "rb") as f_in:
        raw = f_in.read(112)
    data = struct.unpack(fmt, raw)
    data = dict(zip(STATS_KEYS, data))
    return data

def load_run_info_xml(run_info_path):
    """Load a RunInfo.xml into a nested dictionary."""
    run_info_obj = ET.parse(run_info_path)
    run = run_info_obj.find("./Run")
    date = run.find("./Date")
    read_infos = run.findall("./Reads/Read")
    read_attrs = []
    for read_info in read_infos:
        read_attrs.append({
            "Number": int(read_info.attrib["Number"]),
            "NumCycles": int(read_info.attrib["NumCycles"]),
            "IsIndexedRead":  read_info.attrib["IsIndexedRead"]})
    run_info = {
        "run_id": run.attrib["Id"],
        "run_num": run.attrib["Number"],
        "run_flowcell": run.find("./Flowcell").text,
        "run_instrument": run.find("./Instrument").text,
        "run_date": date.text,
        "run_reads": read_attrs}
    return run_info

def make_cycle_attr_map(run_info, with_read_attrs=True, with_run_attrs=False):
    """Make a list of (highly redundant) dictionaries of per-cycle attributes"""
    # First map out per-read (R1/R2/I1/I2) info in terms of cycle rather than
    # read type
    read_layout = []
    before = 0
    part = 0
    while True:
        if len(read_layout) >= run_info["run_reads"][part]["NumCycles"] + before:
            before = len(read_layout)
            part += 1
        try:
            read_layout.append(run_info["run_reads"][part])
        except IndexError:
            break
    # Build up a list with one entry per cycle, with whatever attributes should
    # be included
    cycle_count = sum(attrs["NumCycles"] for attrs in run_info["run_reads"])
    cycle_map = []
    for cycle_idx in range(cycle_count):
        row = {}
        if with_run_attrs:
            row.update({key: run_info[key] for key in RUN_ATTRS_KEYS})
        if with_read_attrs:
            row["read_number"] = read_layout[cycle_idx]["Number"]
            row["read_is_index"] = read_layout[cycle_idx]["IsIndexedRead"]
        cycle_map.append(row)
    return cycle_map

def run_stats_tally(rundir, with_read_attrs=True, with_run_attrs=False):
    """Make a List of per-cycle info for a run."""
    rundir = Path(rundir)
    cycle_map = make_cycle_attr_map(
        load_run_info_xml(rundir/"RunInfo.xml"),
        with_read_attrs, with_run_attrs)
    def justdirs(dirs):
        return [p for p in dirs if p.is_dir()]
    def get_cycle_num(path):
        return int(re.sub(r"C([0-9]+)\.1", r"\1", str(path.name)))
    lane_dirs = sorted(justdirs((rundir/"Data/Intensities/BaseCalls").glob("L*")))
    output = []
    for lane_dir in lane_dirs:
        cycle_dirs = justdirs(lane_dir.glob("*"))
        cycle_dirs = sorted(cycle_dirs, key=get_cycle_num)
        for cycle_dir in cycle_dirs:
            cycle_idx = get_cycle_num(cycle_dir) - 1
            stats_dirs = sorted(cycle_dir.glob("*.stats"))
            for stats_path in stats_dirs:
                lane, tile = re.match("s_([0-9]+)_([0-9]+).stats", stats_path.name).groups()
                tile = re.sub("^s_1_", "", stats_path.stem)
                info = load_bcl_stats(stats_path)
                info.update({"lane": lane, "tile": tile})
                info.update(cycle_map[cycle_idx])
                output.append(info)
    return output

def run_stats_table(rundir, path_out, with_read_attrs=True, with_run_attrs=False, wide=True):
    """Make a CSV table of the per-cycle info for a run."""
    tally = run_stats_tally(rundir, with_read_attrs, with_run_attrs)
    out_fields = ["lane", "tile"]
    if with_run_attrs:
        out_fields += RUN_ATTRS_KEYS
    if with_read_attrs:
        out_fields += READ_ATTRS_KEYS
    if wide:
        out_fields += STATS_KEYS
        with open(path_out, "wt", buffering=1, encoding="ASCII") as f_out:
            writer = DictWriter(f_out, fieldnames=out_fields, lineterminator="\n")
            writer.writeheader()
            writer.writerows(tally)
    else:
        out_fields.append("base")
        for key in STATS_KEYS:
            colname = re.sub("_.$", "", key)
            if colname not in out_fields:
                out_fields.append(colname)
        tally_long = []
        for row in tally:
            for base in ["A", "C", "G", "T", "X"]:
                row_out = {key: val for key, val in row.items() if key in out_fields}
                row_out["base"] = base
                base_keys = [
                    f"avg_int_all_{base}",
                    f"avg_int_cluster_{base}",
                    f"num_clust_call_{base}",
                    f"num_clust_int_{base}"]
                for base_key in base_keys:
                    if base_key in row:
                        colname = re.sub("_.$", "", base_key)
                        row_out[colname] = row[base_key]
                tally_long.append(row_out)
        with open(path_out, "wt", buffering=1, encoding="ASCII") as f_out:
            writer = DictWriter(f_out, fieldnames=out_fields, lineterminator="\n")
            writer.writeheader()
            writer.writerows(tally_long)

def main():
    """Command-line interface for run_stats_tally"""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("rundir", help="path to a run directory")
    parser.add_argument("output", help="path to CSV output")
    parser.add_argument("--read-attrs",
        help="include columns (mostly redundant across rows) "
        "noting per-read (R1/I1/I2/R2) information",
        action=argparse.BooleanOptionalAction, default=True)
    parser.add_argument("--run-attrs",
        help="include columns (completely redundant across rows) "
        "noting per-run information",
        action=argparse.BooleanOptionalAction, default=False)
    group_long_wide = parser.add_mutually_exclusive_group()
    group_long_wide.add_argument("-l", "--long",
        help="one row per lane+cycle+tile+base (each row specific to a base call)",
        action="store_true")
    group_long_wide.add_argument("-w", "--wide",
            help="one row per lane+cycle+tile (base call stats across columns)",
        action="store_true")
    args = parser.parse_args()
    # wide by default, unless args.long is explicitly True
    wide = args.wide or not args.long
    run_stats_table(
        args.rundir, args.output,
        args.read_attrs, args.run_attrs, wide=wide)

if __name__ == "__main__":
    main()
