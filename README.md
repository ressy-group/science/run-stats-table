# Illumina Run Stats Table Maker

This little pure-Python script parses Illumina's individual binary per-cycle
.stats files for a run directory and spits out a single CSV of per-cycle
information like counts of base calls per nucleotide, signal intensity, etc.
Since this is per-cycle it makes no difference which are categorized as
R1/I1/I2/R2 downstream.

This can be used to create summaries similar to [FastQC]'s
[Per Base Sequence Content plots](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/3%20Analysis%20Modules/4%20Per%20Base%20Sequence%20Content.html),
among other things.

**NOTE:** This doesn't handle the .bcl.bzgf-based file layout newer Illumina
sofware creates.

[FastQC]: https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
